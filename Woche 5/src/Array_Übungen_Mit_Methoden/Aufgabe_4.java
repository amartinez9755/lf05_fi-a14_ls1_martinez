package Array_�bungen_Mit_Methoden;

public class Aufgabe_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int zeilen = 20;
		//der Anzahl der Zeilen k�nnte auch mit einem Scanner angegeben werden
		double[][] tabelle = temperatur(zeilen);
		for (int x = 0; x < zeilen; x++) {
			System.out.printf("%.2f F | %.2f C\n", tabelle[x][0], tabelle[x][1]);
		}
	}

	public static double[][] temperatur(int anzahl) {
		double[][] tabelle_2 = new double[anzahl][2];
		tabelle_2[0][0] = 0.0;
		tabelle_2[0][1] = (double) (5) / 9 * (tabelle_2[0][0] - 32);
		for (int y = 1; y < anzahl; y++) {
			tabelle_2[y][0] = tabelle_2[y - 1][0] + 10;
			tabelle_2[y][1] = (double) (5) / 9 * (tabelle_2[y][0] - 32);
		}
		return tabelle_2;
	}

}
