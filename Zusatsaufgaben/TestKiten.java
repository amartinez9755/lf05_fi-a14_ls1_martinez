
public class TestKiten {

	public static void main(String[] args) {
		
		//Erzeugung der Objekten
		Kistenklasse kiste1 = new Kistenklasse();
		Kistenklasse kiste2 = new Kistenklasse();
		Kistenklasse kiste3 = new Kistenklasse();
		
		//Setzen der Methode
		kiste1.setVolumen(1, 1, 1);
		kiste1.setFarbe("Rot");
		
		kiste2.setVolumen(2, 3, 4);
		kiste2.setFarbe("Gruen");
		
		kiste3.setVolumen(2, 2, 2);
		kiste3.setFarbe("Orange");
		
		
		//Ausgaben
		System.out.println("Die erste Kiste ist " + kiste1.getFarbe());
		System.out.println("Die Kiste hat einen Volumen von: " + kiste1.getVolumen() + "cm^3");
	}
	
}
