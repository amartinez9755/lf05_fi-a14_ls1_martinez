import java.util.Scanner;
import java.lang.Math.*;

public class AB_Auswahlstrukturen_Aufgabe6_FunktionsLoeser {

	public static void main(String[] args) {
		
	Scanner kb = new Scanner(System.in);
	
	double e = 2.7182;
	double resultat;
	
	System.out.print("Geben Sie den Wert von x an: ");
	double x = kb.nextDouble();
	
		if (x <= 0) {
			//exponential
			resultat = exponential(e,x);
			System.out.println("Es handelt sich um die exponentielle Funktion: f(x) = e^x");
			System.out.println("Die Loesung der Funktion ist: f(x)=" + resultat);
			
		}
		else if (x > 0 && x <= 3) {
			//quadratisch
			resultat = quadratisch(x);
			System.out.println("Es handelt sich um die quadratische Funktion: f(x)= x^2 + 1 ");
			System.out.println("Die Loesung der Funktion ist: f(x)=" + resultat);
		}
		else {
			//linear 
			resultat = linear(x);
			System.out.println("Es handelt sich um die quadratische Funktion: f(x) = 2x + 4 ");
			System.out.println("Die Loesung der Funktion ist: f(x)=" + resultat);
		}
	}
	
	public static double exponential(double e,double x) {
		double f = Math.pow(e, x);
		return f;
	}
	
	public static double quadratisch(double x) {
		double f = (x*x) + 1;
		return f;
	}
	public static double linear(double x) {
		double f = (x*2) + 4;
		return f;
	}
}
