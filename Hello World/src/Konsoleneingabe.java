import java.util.Scanner; // Import der Klasse Scanner

public class Konsoleneingabe {

	public static void main(String[] args) { // Hier startet das Programm

		// Neues Scanner-Objekt myScanner wird erstellt
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Willkomen!! Bitte geben Sie Ihr Namen ein: ");

		// Die Variable zahl1 speichert die erste Eingabe
		String Name = myScanner.next();

		// Die Variable zahl2 speichert die zweite Eingabe
		System.out.print("Bitte geben Sie Ihr Alter ein: ");
		int Alter = myScanner.nextInt();
		
		// Die Addition der Variablen zahl1 und zahl2
		// wird der Variable ergebnis zugewiesen.
		//int ergebnis = zahl1 + zahl2;

		System.out.print("\n\nIhr Name ist " + Name + " und Sie sind " + Alter + " Jahre alt.");
		myScanner.close();
		
		
	}

}
