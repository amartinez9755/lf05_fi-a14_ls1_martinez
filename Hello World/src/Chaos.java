//Aufgabe 2.1

public class Chaos {
		public static void main(String[] args) {
			
			String bezeichnung = "Q2021_FAB_A";
			String typ = "Automat AVR";
			String name = typ + " " + bezeichnung;
			
			double patrone = 46.24;
			double maximum = 100.00;
			double prozent = maximum - patrone;
		
			int muenzenCent = 1280;
			int muenzenEuro = 130;
			int summe = muenzenCent + muenzenEuro * 100;
			int euro = summe / 100;
			int cent = summe % 100;
			
			char sprachModul = 'd';
			
			final byte PRUEFNR = 4;
			
			boolean status = (euro <= 150) && (prozent >= 50.00)
					&& (euro >= 50) && (cent != 0) && (sprachModul == 'd')
					&&  (!(PRUEFNR == 5 || PRUEFNR == 6)); 
			
			System.out.println("Name: " + name);
			System.out.println("Sprache: " + sprachModul);
			System.out.println("Pruefnummer : " + PRUEFNR);
			System.out.println("Fuellstand: " + prozent + " %");
			System.out.println("Summe Euro: " + euro +  " Euro");
			System.out.println("Summe Rest: " + cent +  " Cent");		
			System.out.println("Status: " + status);
			
	}
}

