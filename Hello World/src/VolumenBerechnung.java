import java.util.Scanner;

public class VolumenBerechnung {

	public static void main(String[] args) {
		
		Scanner MyScanner = new Scanner(System.in);
		
		double a;
		double b;
		double c;
		double h;
		final double PI = 3.1416;
		
		double Volumen;
		
		//Masse des W�rfels
		System.out.println("Geben Sie die L�nge der seiten des W�rfels ein: ");
			a = MyScanner.nextDouble();
			Volumen = wuerfel(a);
			System.out.println("Das Volumen des W�rfels ist: " + Volumen + " cm^2");
			
		//Masse des Quaders			
		System.out.println("Geben Sie die L�nge der seiten des Quaders: ");
			a = MyScanner.nextDouble();
			b = MyScanner.nextDouble();
			c = MyScanner.nextDouble();
			Volumen = quader(a,b,c);
			System.out.println("Das Volumen des Quaders ist: " + Volumen + " cm^2");
		
		//Masse der Pyramide	
		System.out.println("Geben Sie die L�nge der seiten des Pyramidenbodens: ");
		a = MyScanner.nextDouble();
		b = MyScanner.nextDouble();
		System.out.println("Geben sie die H�he der Pyramide ein: ");
		h = MyScanner.nextDouble();
		Volumen = pyramide(a,b,h);
			System.out.println("Das Volumen des Quaders ist: " + Volumen + " cm^2");
		
		//Masse der Kugel
		System.out.println("Geben Sie die L�nge des Radius der Kugel: ");
		a = MyScanner.nextDouble();
			System.out.println("Das Volumen der Kugel ist: " + Volumen + " cm^2");
			Volumen = kugel(a,PI);
	}

	public static double wuerfel(double a) {
		double kubik = a*a*a;
		return kubik;
		
	}
	
	public static double quader(double a, double b, double c) {
		double vol = a*b*c;
		return vol;
	}
	
	public static double pyramide(double a, double b, double h) {
		double volumen = a*b*h;
		return volumen;
	}
	
	public static double kugel(double a, double PI) {
		double volumen = a * PI * 1.333333;
		return volumen;
	}
}
