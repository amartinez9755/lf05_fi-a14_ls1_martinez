
public class AB_Methoden1 {

	public static void main(String[] args) {
		
		double x = 2.37;
		double y = 4.0;
		double z;
		
		z = multiplikation(x, y);
		
		System.out.printf("Das Ergebnis der Multiplikation aus den Zahlen %.2f und %.2f ist: %.2f", x, y, z);
		
	}
	
	public static double multiplikation(double a, double b) {
		double multi = a*b;
		return multi;
		
	}
	
}