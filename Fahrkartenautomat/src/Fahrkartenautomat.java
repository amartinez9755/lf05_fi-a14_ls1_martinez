﻿import java.util.Scanner;

class Fahrkartenautomat {

	static Scanner tastatur = new Scanner(System.in);

	public static double fahrkartenbestellungErfassen() {
		System.out.println("Fahrkartenbestellvorgang:");
		warte(100);
		System.out.println("\n\n");

		int aTickets;
		int wahl = 1;
		double betrag = 0.0;
		double tPreis;
		String aTicketsText = "Anzahl der Tickets (1-10): ";
		String aTicketsWahl = "Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.";

		while (wahl != 9) {
			System.out.println("\nWählen Sie ihre Wunschkarte für Berlin AB aus:");
			System.out.println("	Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
			System.out.println("	Tageskarte Regeltarif AB [8,60 EUR] (2)");
			System.out.println("	Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
			System.out.println("	Bezahlen (9)");
			System.out.print("Ihre Wahl: ");
			wahl = tastatur.nextInt();
			aTickets = 0;
			if (wahl == 1) {
				while (aTickets <= 0 || aTickets > 10) {
					System.out.print(aTicketsText);
					aTickets = tastatur.nextInt();
					if (aTickets > 0 && aTickets <= 10) {
						tPreis = 2.90;
						betrag = betrag + aTickets * tPreis;
						System.out.printf("\nZwischenbetrag: %.2f€\n", betrag);
					} else {
						System.out.println(aTicketsWahl);
					}
				}
			} else if (wahl == 2) {
				while (aTickets <= 0 || aTickets > 10) {
					System.out.print(aTicketsText);
					aTickets = tastatur.nextInt();
					if (aTickets > 0 && aTickets <= 10) {
						tPreis = 8.60;
						betrag = betrag + (aTickets * tPreis);
						System.out.printf("\nZwischenbetrag: %.2f€\n", betrag);
					} else {
						System.out.println(aTicketsWahl);
					}
				}
			} else if (wahl == 3) {
				while (aTickets <= 0 || aTickets > 10) {
					System.out.print(aTicketsText);
					aTickets = tastatur.nextInt();
					if (aTickets > 0 && aTickets <= 10) {
						tPreis = 23.50;
						betrag = betrag + (aTickets * tPreis);
						System.out.printf("\nZwischenbetrag: %.2f€\n", betrag);
					} else {
						System.out.println(aTicketsWahl);
					}
				}
			} else if (wahl == 9) {
				System.out.printf("\nZuzahlender Betrag: %.2f€\n", betrag);
			} else {
				System.out.println("Falsche Eingabe!!");
			}
		}
		return betrag;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		double eingezahlt = 0.0;
		while (eingezahlt < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f Euro.\n", (zuZahlen - eingezahlt));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double muenze = tastatur.nextDouble();
			if (muenze <= 2 && muenze >= 0.5) {
				eingezahlt += muenze;
			} else {
				System.out.println("Bitte gueltige Muenzen einwerfen.\n");
			}
		}
		return eingezahlt;

	}

	/*
	public static double fahrkartenbestellungErfassen_2() {

		double pTickets[] = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };
		String tNamen[] = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC",
				"Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC",
				"Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };

			System.out.println("\nBitte wählen Sie eine Karte aus: ");
			for (int x = 0; x < tNamen.length; x++) {
				System.out.printf("%s: %.2d€ (%f)", tNamen[x], pTickets[x], x);
				warte(30);
			}
			
		}
	}
	*/

	public static void rueckgeldAusgeben(double rückgabebetrag) {
		if (rückgabebetrag > 0.0) {
			System.out.printf("%s %.2f %s", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO \n");
			System.out.println("wird in folgenden Muenzen ausgezahlt:");
			muenzeAusgeben(rückgabebetrag);
		}
	}

	public static void muenzeAusgeben(double rückgabebetrag) {
		while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
		{
			System.out.printf("%8s\n", "* * *");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%s%5s%5s\n", "*", "2", "*");
			System.out.printf("%s%7s%3s\n", "*", "EURO", "*");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%8s\n", "* * *");
			rückgabebetrag -= 2.0;
		}
		while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
		{
			System.out.printf("%8s\n", "* * *");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%s%5s%5s\n", "*", "1", "*");
			System.out.printf("%s%7s%3s\n", "*", "EURO", "*");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%8s\n", "* * *");
			rückgabebetrag -= 1.0;
		}

		while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
		{
			System.out.printf("%8s\n", "* * *");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%s%5s%5s\n", "*", "50", "*");
			System.out.printf("%s%7s%3s\n", "*", "CENT", "*");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%8s\n", "* * *");
			rückgabebetrag -= 0.5;
		}
		while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
		{
			System.out.printf("%8s\n", "* * *");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%s%5s%5s\n", "*", "20", "*");
			System.out.printf("%s%7s%3s\n", "*", "CENT", "*");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%8s\n", "* * *");
			rückgabebetrag -= 0.2;
		}
		while (rückgabebetrag >= 0.09999) // 10 CENT-Münzen
		{
			System.out.printf("%8s\n", "* * *");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%s%5s%5s\n", "*", "10", "*");
			System.out.printf("%s%7s%3s\n", "*", "CENT", "*");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%8s\n", "* * *");
			rückgabebetrag -= 0.1;
		}
		while (rückgabebetrag >= 0.049999)// 5 CENT-Münzen
		{
			System.out.printf("%8s\n", "* * *");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%s%5s%5s\n", "*", "5", "*");
			System.out.printf("%s%7s%3s\n", "*", "CENT", "*");
			System.out.printf("%2s%8s\n", "*", "*");
			System.out.printf("%8s\n", "* * *");
			rückgabebetrag -= 0.05;
		}
	}
	

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben ");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void warte(int wartezeit) {

		for (int i = 0; i < 20; i++) {
			System.out.print("=");
			try {
				Thread.sleep(wartezeit);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}


	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		double rückgabebetrag;

		// Aufruf der Methoden

		zuZahlenderBetrag = fahrkartenbestellungErfassen();

		eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

		fahrkartenAusgeben();

		rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		rueckgeldAusgeben(rückgabebetrag);
		//Graphische Ausgabe der Münzen
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!");
		warte(75);
		System.out.print("\n\n");
		System.out.println("Wir wünschen Ihnen eine gute Fahrt!\n\n");

	}
}

/*
 * A2.5 5. Double wird hier gebracuht, weil in dem Fall Nachkommastellen
 * gebraucht werden 6. Bei der Berechnung von "anzahl*einzelpreis" wird der
 * Preis des einzehlnen Tickets mal der Anzahl der gekauften Tickets
 * multipliziert und die Variable zuZahlenderBetrag zugewiesen.
 */