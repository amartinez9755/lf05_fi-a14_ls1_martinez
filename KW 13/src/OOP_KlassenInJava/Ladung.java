package OOP_KlassenInJava;

public class Ladung {  

    //Attribute 
    private String bezeichnung;
    private int menge;


    //Konstruktoren
    public Ladung(String bezeichnung, int menge) {
        this.bezeichnung = bezeichnung;
        this.menge = menge;
    }
    
    public Ladung() {
    }


    //Getter
    public String getBezeichnung() {
        return bezeichnung;
    }
    public int getMenge() {
        return menge;
    }

    //Setter
    public void setMenge(int menge) {
        this.menge = menge;
    }
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
}