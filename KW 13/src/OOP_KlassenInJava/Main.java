package OOP_KlassenInJava;

import java.util.concurrent.Phaser;

public class Main {
    public static void main (String args[]) {
        //Erzeugen Raumschiffe
        Raumschiff klingonenRaumschiff = new Raumschiff(1,100,100,100,100, "IKS Heghta",2);
        Raumschiff romulaneRaumschiff = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);
        Raumschiff vulkanierRaumschiff = new Raumschiff(0, 80, 80, 50, 100, "Ni Var", 5);


        //Erzeugen Ladungsobjekte 1
        Ladung kl1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung kl2 = new Ladung("Batleth Klingonen Schwert", 200);
        Ladung rl1 = new Ladung("Borg-Schrott", 5);
        Ladung rl2 = new Ladung("Rote Materie", 2);
        Ladung rl3 = new Ladung("Plasma-Waffe", 50);
        Ladung vl1 = new Ladung("Forschungssonde", 35);
        Ladung vl2 = new Ladung("Photonentorpedo", 3);

        //Aktionen
        klingonenRaumschiff.photonentorpedoSchiessen(romulaneRaumschiff);
        romulaneRaumschiff.phaserkanoneSchiessen(klingonenRaumschiff);
        vulkanierRaumschiff.nachrichtAnAlle("Gewalt ist nicht logisch!");
        klingonenRaumschiff.zustandRaumschiff();
        klingonenRaumschiff.ladungverzeichnisAusgeben();
        vulkanierRaumschiff.schiffReparieren(true, true, false, 5); 
        vulkanierRaumschiff.photonentorpedosLaden(50);
        vulkanierRaumschiff.ladungverzeichnisClear(vulkanierRaumschiff);
        klingonenRaumschiff.photonentorpedoSchiessen(romulaneRaumschiff);
        klingonenRaumschiff.photonentorpedoSchiessen(romulaneRaumschiff);
        klingonenRaumschiff.getBroadcastcommunicator();
        romulaneRaumschiff.getBroadcastcommunicator();
        vulkanierRaumschiff.getBroadcastcommunicator();
    }   
}
