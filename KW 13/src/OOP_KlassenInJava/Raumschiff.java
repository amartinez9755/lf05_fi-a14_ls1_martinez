package OOP_KlassenInJava;
import java.util.ArrayList;
import java.util.jar.Attributes.Name;

public class Raumschiff {

	//Attribute
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungverzeichnis = new ArrayList<Ladung>();

	//Konstruktoren
	public Raumschiff(){
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent,String schiffsname,int androidenAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProent = schildeInProent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}


	//Getter
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public int getSchildeInProent() {
		return schildeInProent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}
	
	public ArrayList<Ladung> getLadungverzeichnis() {
		return ladungverzeichnis;
	}

	public void getBroadcastcommunicator(){
		int i = 0;
		while (this.broadcastKommunikator.size() > i){
			if (this.broadcastKommunikator.get(i) != null){
				System.out.printf("Broadcast: %s\n" , 1 + i, this.broadcastKommunikator.get(i));
				i++;
			}

		}
	}

	//Setter
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzentNeu) {
		this.energieversorgungInProzent = energieversorgungInProzentNeu;
	}

	public void setSchildeInProent(int schildeInProentNeu) {
		this.schildeInProent = schildeInProentNeu;
	}

	public void setHuelleInProzent(int huelleInProzentNeu) {
		this.huelleInProzent = huelleInProzentNeu;
	}
	
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzentNeu;
	}

	public void setAndroidenAnzahl(int androidenAnzahlNeu) {
		this.androidenAnzahl = androidenAnzahlNeu;
	}

	public void setSchiffsname(String schiffsnameNeu){
		this.schiffsname = schiffsnameNeu;
	}

	public void setLadungverzeichnis(ArrayList<Ladung> ladungverzeichnis) {
		this.ladungverzeichnis = ladungverzeichnis;
	}


	//Methoden
	public void phaserkanoneSchiessen(Raumschiff zielRaumschiff) {
		if (getEnergieversorgungInProzent() <= 50){
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			System.out.println("Phaserkanone abgeschossen!");
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);
			treffer(zielRaumschiff);
		}
	}

	public void photonentorpedoSchiessen(Raumschiff zielRaumschiff) {
		if (getPhotonentorpedoAnzahl() <= 0){
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			System.out.println("Photonentorpedo abgeschossen!!");
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() - 1);
			treffer(zielRaumschiff);
		}
	}

	public void addLdung(Ladung neueLadung) {
		this.ladungverzeichnis.add(neueLadung);
	}

	private void treffer(Raumschiff getroffenesSchiff){
		String strokenTarget = getroffenesSchiff.getSchiffsname() + "wurde getroffen!!";
		nachrichtAnAlle(strokenTarget);
	}

	public void nachrichtAnAlle(String nachricht){
		broadcastKommunikator.add(nachricht);
		System.out.println(nachricht);
	}

	public void photonentorpedosLaden(int anzahlTorpedos) {
		setPhotonentorpedoAnzahl(anzahlTorpedos);
	}

	public void schiffReparieren(boolean schutzschilde, boolean energieversorgung, boolean huelle, int androidenAnzahl) {
		if (schutzschilde = true) {
			setSchildeInProent(100);
		}
		if (energieversorgung = true){
			setEnergieversorgungInProzent(100);
		}
		if (huelle = true){
			setHuelleInProzent(100);
		}
		if(androidenAnzahl > 0){
			setAndroidenAnzahl(androidenAnzahl);
		}
	}

	public void zustandRaumschiff() {
		System.out.println("Zustand des Schiffes : '" + getSchiffsname() + "' ");
		System.out.println("Anzahl der Photonenthorpedos: " + getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung in Prozent: " + getEnergieversorgungInProzent() + "%");
		System.out.println("Lebensunterhaltungssysteme in Prozent: " + getLebenserhaltungssystemeInProzent() + "%");	
		System.out.println("Schiffshuelle in Prozent: " + getHuelleInProzent() + "%");
		System.out.println("Zustand der Schilder in Prozent: " + getSchildeInProent() + "%");
	}

	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		System.out.println("Anzahl der Eintraege im Logbuch: " + broadcastKommunikator.size());
		System.out.println("--------------------------");
		return broadcastKommunikator;
	}

	public void ladungverzeichnisAusgeben() {
		int counter = 0;
		while(this.ladungverzeichnis.size() > counter){
			if(this.ladungverzeichnis.get(counter) != null){
				System.out.printf("Ladung %d: %d %s \n", 1 + counter , this,ladungverzeichnis.get(counter).getMenge(),
				this.ladungverzeichnis.get(counter).getBezeichnung());
				counter++;
			}
		}
	}

	public void ladungverzeichnisClear(Raumschiff name) {
		name.getLadungverzeichnis().clear();
	}
}
